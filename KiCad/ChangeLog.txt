Changelog revision 2.1:
Change C442,C444,C445,C448 to 5mm film types - 3.5mm width

Schematic changelog revision 2.0:
Change D301/D302 to SS14 (SOD123)
Change D303,D304,D306,D307,D308,D310 to SS14 (SOD123)
Update all headers for 1mm holes
Change mounting holes and added fiducials
Change U301 to TLV1117LV33DCY
Change U302 to TLV70025DDC
Changed P206 to 2x6 with SPI connections only
Removed R232
Moved R226, R227, R228, R229, R230, R231
Change R226 to 75ohm
Change C446, C447, C450, C451 to 0805
Remove P402, P403
Change P404-P409 to 2 pin 
Change P401. P404-P409 to JP reference
Remove R403,R404,R406, R408, R410, R413, R415, R417
Move R401
Updated all parts with extra details/fields

PCB changelog revision 2.0:
Update all box headers for 1mm holes
Updated ADAU1966 footprint with pin 1 marker
Change mounting holes
Added fiducials
Change P206 to 2x6 with SPI only
Change U301 to SOT223
Change U302 to SOT23-5
Moved U301, U302, U303 and all parts left
Moved power connectors and added labels
Spaced out capacitors around U402